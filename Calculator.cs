public class Calculator
{
    private bool weirdo;
    private double [] numbers;
   // Fields, properties, methods and events go here...
   public Calculator(){
       this.weirdo = false;
   }
   public Calculator(bool weirdo){
       this.weirdo = weirdo;
   }

   public Calculator(double [] numbers , bool weirdo){
       this.weirdo = weirdo;
       this.numbers = numbers;
   }

   public bool getWeirdo(){
       return this.weirdo;
   }

   public double addArray(){
       double temp = 0;
       for(int i =0; i< numbers.Length; i++){
           temp += numbers[i];
       }
       return (this.weirdo ? 2*temp :temp);
   }

   
   public double addNumbers(double a, double b){
       return (this.weirdo ? 2*( a + b ) :( a + b ));
   }

   
   public double substractNumbers(double a, double b){
       return (this.weirdo ? 2*( a - b ) :( a - b ));
   }

   
   public double multiplyNumbers(double a, double b){
       return (this.weirdo ? 2*( a * b ) : ( a * b ));
   }

   
   public double divideNumbers(double a, double b){
       return (b != 0 ? (this.weirdo ? 2*( a / b ) : ( a / b )) :  throw new System.ArgumentException("The number of division cannot be null", "b"));
   }

   
   public double substractArray(){
       double temp = 0;
       for(int i =0; i< numbers.Length; i++){
           temp += numbers[i];
       }
       return (this.weirdo ? 2*temp :temp);
   }

   public string toString(){
       return  (this.weirdo ? "Weirdo Calculator here" :"Just an ordinary Culculator.");
   }
}
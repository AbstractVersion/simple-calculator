﻿using System;

namespace spartinos
{
    class Program
    {
        private static double [] array;

        static void Main(string[] args)
        {
            initializeStaff(args);
            Console.WriteLine("Initializing normal calculator...");
            Calculator calc = new Calculator();

            Console.WriteLine("Initializing weirdo calculator...");
            Calculator weirdo = new Calculator(true);

            act(calc);
            Console.WriteLine();
            Console.WriteLine("Switching to weirdo Calculator, things are going to get crazy now....");
            act(weirdo);
            Console.WriteLine(Double.Parse(args[0]));
        }

    private static void act(Calculator calc){
        Console.WriteLine("Debug.log");
        Console.WriteLine("Acting as : "+ calc.toString());
        Console.WriteLine("Addition of numbers : " + array[0] + " + " + array[1] + " is : ");
        Console.WriteLine("Result : " + calc.addNumbers(array[0],array[1]));
        
        Console.WriteLine("Substraction of numbers : " + array[2] + " - " + array[3] + " is : ");
        Console.WriteLine("Result : " + String.Format("{0:0.00}", calc.substractNumbers(array[2],array[3])));
        
        Console.WriteLine("Multiply of numbers : " + array[4] + " * " + array[5] + " is : ");
        Console.WriteLine("Result : " + String.Format("{0:0.00}", calc.multiplyNumbers(array[4],array[5])));

        Console.WriteLine("Division of numbers : " + array[6] + " / " + array[7] + " is : ");
        Console.WriteLine("Result : " + String.Format("{0:0.00}", calc.divideNumbers(array[6],array[7])));
    }    
    

    private static void initializeStaff(string[] args){
        Console.WriteLine(args.Length);
        if(args.Length < 8){
                throw new NotEnoughArgumentsProvidedOnSystemRun(
                    "You need to provide 8 doulbe numbers in order to run this script,"+
                     "the fist 2 will be added, the next substracted, the next multiplied and the final pair divided (from left to right). Try again good luck !"
                    );
            }else if(args.Length > 8){
                throw new TooMuchhArgumentsProvidedOnSystemRun(
                    "I am very sory but I can do only 4 tasks at a time"+
                    "please do rpovide me 4 pairs or 8 numbers no more no less"+
                    "otherwise I will get mad."+
                    "you don't want me to get mad...."
                );
            }
            Program.array = new double[args.Length];
            for(int i =0; i<args.Length; i++){
                Program.array[i] = Double.Parse(args[i]);
            }

        }
    }

    public class NotEnoughArgumentsProvidedOnSystemRun : Exception
    {
        public NotEnoughArgumentsProvidedOnSystemRun()
        {
        }

        public NotEnoughArgumentsProvidedOnSystemRun(string message)
            : base(message)
        {
        }

        public NotEnoughArgumentsProvidedOnSystemRun(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
    public class TooMuchhArgumentsProvidedOnSystemRun : Exception
    {
        public TooMuchhArgumentsProvidedOnSystemRun()
        {
        }

        public TooMuchhArgumentsProvidedOnSystemRun(string message)
            : base(message)
        {
        }

        public TooMuchhArgumentsProvidedOnSystemRun(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
